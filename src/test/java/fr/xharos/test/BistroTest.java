package fr.xharos.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * File <b>BistroTest</b> located on fr.xharos.test
 * BistroTest is a part of Bistro.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Bistro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 08/07/2017 at 17:13
 * @since 1.0.0
 */
public class BistroTest {

    @Test
    public void testStringToInt() {
        assertTrue(123 == stringToInt("123toto"));
    }

    @Test
    public void testIntToString() {
        assertTrue("12".equals(intToString(12)));
    }

    @Test
    public void testIntToStringIterative() {
        assertTrue("-1256".equals(intToStringIterative(-1256, null)));
    }

    @Test
    public void testBaseToBase() {
        assertTrue("1100".equals(stringToBase("12", "0123456789", "01")));
        assertTrue("HO".equals(stringToBase("10", "0123456789", "XHAROS")));
    }

    private String intToString(int n) {
        StringBuilder sb = new StringBuilder();
        int div = 1;

        if (n < 0)
            sb.append('-');
        else
            n = -n;
        while (n / div < -9)
            div *= 10;
        while (div > 0) {
            sb.append((char) ('0' - (n / div) % 10));
            div /= 10;
        }
        return sb.toString();
    }

    private int stringToInt(String s) {
        int len = s.length();
        int i = 0;
        int num = 0;
        int neg = 1;

        while (i < len && Character.isWhitespace(s.charAt(i)))
            ++i;
        if (s.charAt(i) == '+' || s.charAt(i) == '-')
            neg = s.charAt(i++) == '+' ? 1 : -1;
        while (i < len && Character.isDigit(s.charAt(i))) {
            num *= 10;
            num += getNumericValue(s.charAt(i++));
        }
        return num * neg;
    }

    private String intToStringIterative(int n, String s) {
        if (s == null)
            s = "";
        if (n < 0)
            s += "-";
        if (n <= -10 || n >= 10)
            s = intToStringIterative((n / 10) * (n < 0 ? -1 : 1), s);
        return s + "" + (char) ('0' + n % 10 * (n < 0 ? -1 : 1));
    }

    private int stringToIntBase(String s, String base) {
        int n = 0;
        int i = 0;
        int sign = 1;
        int base_len = base.length();

        if (i < s.length() && (s.charAt(i) == '+' || s.charAt(i) == '-'))
            sign = s.charAt(i++) == '-' ? -1 : 1;
        while (i < s.length())
            n = n * base_len + base.indexOf(s.charAt(i++));
        return n * sign;
    }

    private String intToStringBase(int n, String base) {
        int div = 1;
        int len = base.length();
        String s = "";

        if (n < 0)
            s += '-';
        else
            n *= -1;
        while (n / div <= -len)
            div *= len;
        while (div > 0) {
            s += base.charAt(-((n / div) % len));
            div /= len;
        }
        return s;
    }

    private String stringToBase(String s, String inBase, String outBase) {
        return intToStringBase(stringToIntBase(s, inBase), outBase);
    }

    private int getNumericValue(char c) {
        return c - '0';
    }
}
