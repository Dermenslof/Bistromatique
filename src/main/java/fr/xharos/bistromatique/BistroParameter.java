package fr.xharos.bistromatique;

import fr.xharos.bistromatique.parameter.base.BaseValidator;
import fr.xharos.bistromatique.parameter.expression.ExpressionValidator;
import fr.xharos.bistromatique.parameter.symbol.SymbolValidator;

/**
 * File <b>BistroParameter</b> located on fr.xharos.bistromatique
 * BistroParameter is a part of Bistromatique.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Bistromatique is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 08/07/2017 at 17:25
 * @since 1.0.0
 */
public class BistroParameter {

    private static BistroParameter     instance;
    private final  String[]            inputValues;
    private final  SymbolValidator     symbol;
    private final  BaseValidator       base;
    private final  ExpressionValidator expression;

    public BistroParameter(String[] inputValues) {
        if (instance == null)
            instance = this;
        this.symbol = new SymbolValidator();
        this.base = new BaseValidator(this);
        this.expression = new ExpressionValidator(this);
        this.inputValues = inputValues;
    }

    public static BistroParameter getInstance() {
        return instance;
    }

    public void validateInputArgs() throws UnsupportedOperationException {
        symbol.validate(inputValues[1]);
        base.validate(inputValues[0]);
        expression.validate(inputValues[2]);
    }

    /**
     * Can be null
     */
    public String getBase() {
        return inputValues[0];
    }

    public String getExpression() {
        return inputValues[2];
    }

    public SymbolValidator getSymbol() {
        return symbol;
    }
}
