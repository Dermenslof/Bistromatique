package fr.xharos.bistromatique.parameter.symbol;

import fr.xharos.bistromatique.token.Token;

/**
 * File <b>SymbolToken</b> located on fr.xharos.bistromatique.parameter.symbol
 * SymbolToken is a part of Bistromatique.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Bistromatique is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 08/07/2017 at 17:35
 * @since 1.0.0
 */
public class SymbolToken extends Token {

    private final SymbolType type;

    public SymbolToken(SymbolType type, String value) {
        super(value, type.getLevel());
        this.type = type;
    }

    public SymbolType getType() {
        return type;
    }

}
