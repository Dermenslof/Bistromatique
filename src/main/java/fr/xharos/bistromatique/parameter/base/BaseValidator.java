package fr.xharos.bistromatique.parameter.base;

import fr.xharos.bistromatique.BistroParameter;
import fr.xharos.bistromatique.functional.Validator;

/**
 * File <b>BaseValidator</b> located on fr.xharos.bistromatique.parameter.base
 * BaseValidator is a part of Bistromatique.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Bistromatique is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 08/07/2017 at 17:26
 * @since 1.0.0
 */
public class BaseValidator implements Validator<Boolean, String> {

    private final BistroParameter parameter;
    private       String          inputBase;

    public BaseValidator(BistroParameter parameter) {
        this.parameter = parameter;
    }

    @Override
    public Boolean validate(String param) {
        if (param == null || param.isEmpty())
            return false;
        System.out.println("-- Start base registration -- ");
        int baseLength = param.length();
        for (int i = 0; i < baseLength; i++) {
            String value = String.valueOf(param.charAt(i));
            if (!valueExist(value, param.substring(0,i))) {
                System.out.println("Register base value " + value + " at position " + i);
            } else
                throw new UnsupportedOperationException("Value " + value + " is already defined for another symbol!");
        }
        this.inputBase = param;
        System.out.println();
        return true;
    }

    private boolean valueExist(String baseValue, String base) {
        return parameter.getSymbol().getSymbolTokenList().stream().anyMatch((tok) -> tok.getValue().equalsIgnoreCase(baseValue)) ||
               base.contains(baseValue);
    }

    public String getInputBase() {
        return inputBase;
    }
}
